# Copyright 2019 LoveIsGrief
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

import os
import sys

if os.name == "nt" or sys.platform == "darwin":
    from quodlibet.plugins import PluginNotSupportedError

    raise PluginNotSupportedError

import dbus

from quodlibet import _, print_e, print_d, commands, app
from quodlibet.qltk import Icons
from quodlibet.plugins.events import EventPlugin


class QLDBusCommand(dbus.service.Object):
    BUS_NAME = "net.sacredchao.QuodLibetExtra"
    PATH = '/net/sacredchao/QuodLibetExtra'
    ROOT_IFACE = BUS_NAME

    @dbus.service.method(ROOT_IFACE, in_signature='s')
    def Rate(self, rating: str):
        commands._rating(app, rating)


class DbusCommands(EventPlugin):
    PLUGIN_ID = "dbus_commands"
    PLUGIN_NAME = _("DBUS Commands")
    PLUGIN_DESC = _("Provides CLI commands over DBUS")
    PLUGIN_ICON = Icons.NETWORK_WORKGROUP

    def __init__(self):
        super(DbusCommands, self).__init__()
        self.dbus_service = None

    def enabled(self):
        try:
            session_bus = dbus.SessionBus()
            name = dbus.service.BusName(QLDBusCommand.BUS_NAME, session_bus)
            self.dbus_service = QLDBusCommand(
                conn=session_bus,
                object_path=QLDBusCommand.PATH,
                bus_name=name
            )
            print_d("Created dbus service: %s" % name)
        except dbus.DBusException as dbe:
            print_e("Couldn't start service: %s" % dbe)

    def disabled(self):
        if self.dbus_service:
            self.dbus_service.remove_from_connection()
