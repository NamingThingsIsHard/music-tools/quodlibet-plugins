# quodlibet-plugins

To "install" these plugins, simply copy them to your plugins directory.
On linux that's most like likely `~/.quodlibet/plugins`

You can then restart quodlibet or reload plugins.
