"""
quodlibet-plugins
Copyright (C) 2016 Ryan Dellenbaugh
Copyright (C) 2017 Nick Boultbee
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from quodlibet import _, print_d, print_w
from quodlibet.plugins.query import QueryPlugin, QueryPluginError

class UniqueQuery(QueryPlugin):
    PLUGIN_ID = "unique_query"
    PLUGIN_NAME = _("Unique Query")
    PLUGIN_DESC = _("Filter search results by unique tags")
    key = 'unique'
    query_syntax = _("@(unique: tag)")
    query_description = _(
        "<tt>tag</tt> can be album, artist, title or any other tag "
    )
    usage = f"{query_syntax}\n\n{query_description}"
    field_set = set()

    def __init__(self):
        print_d("Initialising")
        self._reported = set()

    def search(self, data, body):
        print(self)
        return_value = False
        try:
            field_value = data[body]
            return_value = field_value not in self.field_set
            self.field_set.add(field_value)
            print_d(f"unique filtering value '{field_value}': {return_value}")
        except KeyError:
            pass
        except Exception as e:
            key = str(e)
            if key not in self._reported:
                self._reported.add(key)
                print_w("%s(%s) in expression '%s'. "
                        "Example failing data: %s"
                        % (type(e).__name__, key, self._raw_body,
                           self._globals))
        return return_value

    def parse_body(self, body):
        self.field_set.clear()
        if body is None:
            raise QueryPluginError
        unique_field = body.strip()
        print_d(f"unique filtering field: {unique_field}")
        self._reported.clear()
        return unique_field
