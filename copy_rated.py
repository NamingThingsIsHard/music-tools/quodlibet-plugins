# Copyright 2019 LoveIsGrief
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
import shutil
from os.path import isdir
from pathlib import Path

from gi.repository import Gtk

from quodlibet import _, print_d, print_w
from quodlibet import qltk
from quodlibet.plugins import PluginConfig
from quodlibet.plugins.events import EventPlugin
from quodlibet.qltk.entry import ValidatingEntry

ATTR_RATING = '~#rating'
ATTR_FILENAME = '~filename'
ATTR_MOUNTPOINT = '~filename'

VERSION = "0.1"

plugin_config = PluginConfig("copy_rated")
defaults = plugin_config.defaults
defaults.set("start_dir_depth", 0)
defaults.set("directory", "")
defaults.set("min_rating", 0)


class CopyRatedPlugin(EventPlugin):
    PLUGIN_ID = "copy_rated"
    VERSION = VERSION
    PLUGIN_NAME = _("Copy rated")
    PLUGIN_DESC = _("Copies rated songs to a defined location")

    def __init__(self):
        super(CopyRatedPlugin, self).__init__()

    def plugin_on_changed(self, songs):
        min_rating = int(plugin_config.get("min_rating", 0))
        for song in songs:
            rating = song.get(ATTR_RATING, 0)
            try:
                rating = int(float(rating) * 100)
            except:
                print_w("Couldn't get rating")
                continue
            try:
                if rating < min_rating:
                    self._del_copied(song)
                else:
                    self._copy_song(song)
            except Exception as e:
                print_w(e)

    def plugin_on_removed(self, songs):
        for song in songs:
            try:
                self._del_copied(song)
            except Exception as e:
                print_w(e)

    def _del_copied(self, song):
        _, target_file = self._build_target_file(song)
        if target_file.exists():
            print_d("Removing copied file %s" % target_file)
            target_file.unlink()
            self._clean_empty(target_file.parent)

    def _clean_empty(self, directory):
        """
        :type directory: Path
        """
        if len(list(directory.iterdir())) == 0:
            print_d("Cleaning empty dir %s" % directory)
            directory.rmdir()
            self._clean_empty(directory.parent)

    def _copy_song(self, song):
        song_file, target_file = self._build_target_file(song)
        if target_file.exists():
            return
        target_dir = target_file.parent
        print_d("Copying song %s to %s " % (song_file, target_dir))
        target_dir.mkdir(parents=True, exist_ok=True)
        shutil.copy(song_file, target_dir)

    def _build_target_file(self, song):
        directory = Path(plugin_config.get("directory"))
        if not (
                directory.is_absolute() or
                directory.is_dir()
        ):
            raise ValueError("Invalid directory", directory)
        song_file = Path(song["~filename"])
        start_dir_depth = int(plugin_config.get("start_dir_depth")) + 1
        parents = song_file.parent.parts
        if len(parents) > start_dir_depth:
            parents = parents[start_dir_depth:]

        target_file = Path(directory, *parents) / song_file.name
        if song_file == target_file:
            raise ValueError("Target file cannot be the same as the song file")

        return song_file, target_file

    def PluginPreferences(self, parent):
        def changed(entry, key):
            if entry.get_property('sensitive'):
                plugin_config.set(key, entry.get_text())

        box = Gtk.VBox(spacing=12)

        # first frame
        table = Gtk.Table(n_rows=5, n_columns=2)
        table.props.expand = False
        table.set_col_spacings(6)
        table.set_row_spacings(6)

        labels = []

        label_names = [
            _("Start directory depth:"),
            _("Target directory:"),
            _("Min rating:")
        ]
        for idx, name in enumerate(label_names):
            label = Gtk.Label(label=name)
            label.set_alignment(0.0, 0.5)
            label.set_use_underline(True)
            table.attach(label, 0, 1, idx, idx + 1,
                         xoptions=Gtk.AttachOptions.FILL |
                                  Gtk.AttachOptions.SHRINK)
            labels.append(label)

        row = 0

        def is_int(i):
            try:
                int(i)
                return True
            except:
                return False

        # Start directory depth
        entry = ValidatingEntry(is_int)
        entry.set_text(plugin_config.get('start_dir_depth'))
        # entry.set_visibility(False)
        entry.connect('changed', changed, 'start_dir_depth')
        table.attach(entry, 1, 2, row, row + 1)
        labels[row].set_mnemonic_widget(entry)
        row += 1

        # Directory
        entry = ValidatingEntry(isdir)
        entry.set_text(plugin_config.get('directory'))
        entry.connect('changed', changed, 'directory')
        table.attach(entry, 1, 2, row, row + 1)
        labels[row].set_mnemonic_widget(entry)
        row += 1

        # Minimum rating
        entry = ValidatingEntry(is_int)
        entry.set_text(plugin_config.get('min_rating'))
        # entry.set_visibility(False)
        entry.connect('changed', changed, 'min_rating')
        table.attach(entry, 1, 2, row, row + 1)
        labels[row].set_mnemonic_widget(entry)
        row += 1

        box.pack_start(qltk.Frame(_("Settings"), child=table), True, True, 0)

        return box
